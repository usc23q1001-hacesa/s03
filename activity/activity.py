year = input("Please input a year: ")
if year[0] == '-':
     print(f"{year} cannot be a negative value")
elif not year.isdigit():
    print(f"{year} is not a number.")
else: 
    year = int(year)
    if year <= 0:
        print(f"{year} cannot be zero or less than zero")
    # A year is considered a leap year if it is divisible by 
    # 400 or 100 (a Century year), i.e., the years 1700, 1800, 1900 
    # are not a leap year, or by 4 (not a Century year), i.e.,
    # 2004, 2008, 2012 are leap years but 2002, 2003, 2005 are not.
    elif year % 4 == 0 and (year % 100 != 0 or year % 400 == 0):
        print(f"{year} is a leap year")
    else:
        print(f"{year} is not a leap year")


rows = int(input("Enter number of rows: "))
cols = int(input("Enter number of cols: "))
for row in range(rows):
    print("*" * cols)

